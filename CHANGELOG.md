## v0.0.2 -> v0.0.3

* Added dummy routes to find-doctor page.

## v0.0.3 -> v0.0.4

* Basic static design improvement of Find Doctor page, Card UI added.
* Removed unnecessary test routes.
* Created Patient Provider to serve Patient View Needs
* Created Search function in the provider. (Handles all cases, but searches only by name)
* Added search by name functionality on front-end (in-sync with backend)
* Consult Page Data sending done, design pending. (More Data has to be sent according to Schema, awaiting doctor view)

## v0.0.4 -> v0.0.5

* Login (UI Change)
* Prescriptions Page - Current & Past tabs created; pages pending
* Orders Page - Offers, Pending & Past tabs created; page design pending
* Search Results refined (Added options to call, consult or view more info for a doctor from search bar)
* Page to view more info on doctor created with 2 tabs (info & review). Design has dummy elements, have to fetch from API. (Waiting for doctor app to progress)
* Custom Doctor Info Header designed for More Doctor Info page and consult page
* Whole appication beautified with color scheme according to design

## v0.0.5 -> v0.0.5.1

* Minor Updates.
* Routes to consult page fixed.
* Committing before experimenting with date pickers, for backup. :P

## v0.0.5.1 -> 0.0.6

* Added test page. Currently, used to sign doctors up (used for backend testing)
* Added test provider to associate with testpage (Handles backend processings of doctor addition)
* Search is now not casesensitive! So "Doc Tor" and "doc Tor" will return proper results.
* More-info page -> Doctor-info has information retrieved from database instead of static info
* The workplaces tiles have locations of places where the doctor works and are dynamic

## v0.0.6 -> v0.0.7

* Appointments can be successfully booked.
* Appointments can now be viewed in the homepage
* Added function to retrieve appointments from database in Patient Provider
* Move consult() function from testing provider to patient provider

## v0.0.7 -> v0.0.8

* ion-datetime used instead of traditional date and time pickers (in Consult page)
* In the home page, appointments now have a button to view more. This opens a modal which can cancel the appointment or see more info
* Ability to cancel appointment. (Appointment status is changed to cancelled in the backend)
* Cancelled appointments dont show up in home page
* Added Appointments Page
* Segragation of appointments into past and present is done using the date. Past appointments are shown in past page, present in present.

## v0.0.8 -> v0.0.9

* Segragation of appointments into past and present is done using date and time.
* In the home, only future appointments (Pending and Accepted) show
* Basic prescription UI done
* Provider functionality to retireve prescriptions of a patient added.
* Fixed Bug : Home showed "done" appointments too.
* Completed viewPrescription page.
* Closed workflow from Patient end towards doctor and back.

## v0.0.9 -> v0.0.10

* Fixed date-time bug on Android
* Fixed date-time picker, set to local time instead of GMT everywhere
* Added <ion-datetime> picker to signup page.

# v0.0.10g -> v0.0.11

* CSS changes in Login and Home Page
* Added loaders while logging in, searching for doctors, retrieving prescriptions, etc
* Written provider function to get history of a patient
* The history in the search page in the find-doctor page is now dynamic and from the DB
* Support for custom icons from png (https://forum.ionicframework.com/t/anyway-to-custom-the-tabbars-icon-with-my-own-svg-file/46131/36)
* Redesigned front page to match design + responsive ness

# v0.0.11 -> v0.0.12

* Major UI changes in Home page. Redesigned with responsive tile-based UI as in the initial design documentation
* Changed card element that displays doctor info in prescription page from <div> to <ion-grid>. Results in more responsive, scalable and easy to view UI designs.
* Redesign workplace-info page to use <ion-grid>
* Redesigned Search page
* Redesigned Consult Page
* Redesigned FindDoctor Page


# v0.0.12 -> v0.0.13

* Fixed Appointment time bug (Used Momentjs Library for time as well) (Bug #5)
* Fixed Bug #4
* CSS changes in ConsultPage and Doctor-info page
* CSS changes in home page
* Nullified emergency button in home page temporarily.
* Added a side-menu
* Logout moved to side menu! Clearer UI/UX

