# Bug 1

## Logic

Prescription.ts is a page that has two tabs - current and past. The current tab loads prescription-current page and the past tab loads the prescription-past page. In the Prescription.ts page, we have a function that will call the Patient Provider to get all the prescriptions and store it in a variable called `prescriptions`. Since the <ion-tab> element doesn't render the root page, when prescription.html is loaded, it called prescription-current.html immediately. Due to the time lag being very less, this happens before the `prescriptions` array is filled. So initially, an empty array goes to the prescription-current page.

## Bug

In the Patient Provider, if we do assignment, then this is not carry forwarded. For example, if we do something like

```
this.patientService.getPrescriptions().then((res) => {
      this.prescriptions = res;
      console.log(this.prescriptions);
    }, (err) => {
      console.log(err)
    })
  }
```

and this assignment happens AFTER the prescription-current page is rendered, the element isn't changed in the prescription-current page.

But, if we did something like this :

```
this.patientService.getPrescriptions().then((res) => {
      for(var x = 0; x < res.length; x++){
        this.prescriptions.push(res[x]);
      }
      console.log(this.prescriptions);
    }, (err) => {
      console.log(err)
    })
  }
  ```

where we push every element of response to this.prescriptions, the change is reflected.

# Bug #3

Why is <ion-grid> being colored to navColor automatically?

Solution : Every SCSS file begins with a selector called page-name{}
CSS for that page, must be within this selector. If its outside, it is considered as a global css thing.

# Bug #4

In the find doctor page, clicking on input messes the whole UI.
Suspect : %ages given to elements.
Cause : ion-grid was given 90% height. 90% of available screen is very short when keyboard is there and hence messes up UI.

# Bug #5

Time Bug

Javascript date library outputs date as 3:57 PM for 15:57 and as 10:57 AM for 10:57. 
We took the substring of the whole date and because of this inconsistency, of where the substring begins we had date errors.
Example : 
If date is like 31/08/2018 10:57AM, I have to take substring from index 11 until (but excluding) 16
But if date is like 31/08/2018 3:57 PM, I have to take index from 11 unti (but excluding) 15

Solution : Use momentJS library to convert time given by JavaScript into standard format of HH:mm (24hr format)

