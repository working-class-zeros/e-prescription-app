

// import { Component } from '@angular/core';
// import { Platform } from 'ionic-angular';
// import { StatusBar } from '@ionic-native/status-bar';

// import { LoginPage } from '../pages/login/login';

// @Component({
//   template: `<ion-nav [root]="rootPage"></ion-nav>`
// })
// export class MyApp {
//   rootPage = LoginPage;
//   // rootPage = FindDoctorPage;
//   constructor(platform: Platform, statusBar: StatusBar) {
//     platform.ready().then(() => {
//       statusBar.styleDefault();
//       // splashScreen.hide();
//     });
//   }
// }import { Component, ViewChild } from '@angular/core';
import { Component, ViewChild } from '@angular/core';
import { App } from 'ionic-angular';

import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { EmergencyPage } from '../pages/emergency/emergency';
import { LoginPage } from '../pages/login/login';
import { AuthProvider } from '../providers/auth/auth';
import { FcmProvider } from '../providers/fcm/fcm';
import { ToastController } from 'ionic-angular';
import { tap } from 'rxjs/operators';
import { Firebase } from '@ionic-native/firebase';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = LoginPage;

  pages: Array<{ title: string, component: any }>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen,
    public authService: AuthProvider, public app: App, fcm: FcmProvider,
    toastCtrl: ToastController,
    public firebaseNative: Firebase) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage },
      // { title: 'Emergency', component: EmergencyPage}
      // { title: 'Logout', component: Log}
    ];

    fcm.listenToNotifications().pipe(
      tap(msg => {
        // show a toast
        const toast = toastCtrl.create({
          message: msg.body,
          duration: 3000
        });
        toast.present();
      })
    )
      .subscribe();

  }


  logout() {
    this.authService.logout();
    // this.nav.setRoot(EmergencyPage)
    this.nav.setRoot(LoginPage);

    // this.nav.remove(0, this.nav.length() - 1);
    // this.nav.insert(0, EmergencyPage);
    // this.nav.pop();
  }
  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      // this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
