import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicStorageModule } from '@ionic/storage';
import { MyApp } from './app.component';
import { AuthProvider } from '../providers/auth/auth';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http'

// My Pages
import { LoginPageModule } from '../pages/login/login.module';
import { SignupPageModule } from '../pages/signup/signup.module';
import { HomePage } from '../pages/home/home';
import {PrescriptionPageModule} from '../pages/prescription/prescription.module';
import {FindDoctorPageModule} from '../pages/find-doctor/find-doctor.module';
import {OrdersPageModule} from '../pages/orders/orders.module';
import {EmergencyPageModule} from '../pages/emergency/emergency.module';
import {SearchPageModule} from '../pages/search/search.module';
import {DoctorMoreInfoPageModule} from '../pages/doctor-more-info/doctor-more-info.module'
import {ConsultPageModule} from '../pages/consult/consult.module';
import {PrescriptionCurrentPageModule} from '../pages/prescription-current/prescription-current.module';
import {PrescriptionPastPageModule} from '../pages/prescription-past/prescription-past.module';
import {OrdersOffersPageModule} from '../pages/orders-offers/orders-offers.module';
import {OrdersPendingPageModule} from '../pages/orders-pending/orders-pending.module';
import {OrdersPastPageModule} from '../pages/orders-past/orders-past.module';
import {DoctorInfoPageModule} from '../pages/doctor-info/doctor-info.module';
import {DoctorReviewPageModule} from '../pages/doctor-review/doctor-review.module';
import {ConfirmDoctorPageModule} from '../pages/confirm-doctor/confirm-doctor.module';
import { PatientProvider } from '../providers/patient/patient';
import {WorkplaceInfoPageModule} from '../pages/workplace-info/workplace-info.module';
// import {AppointmentInfoModalPage} from '../pages/appointment-info-modal/appointment-info-modal'
import {AppointmentInfoModalPageModule} from '../pages/appointment-info-modal/appointment-info-modal.module'
import { AppointmentsPageModule } from '../pages/appointments/appointments.module';
import {CurrentAppointmentsPageModule} from '../pages/current-appointments/current-appointments.module';
import {PastAppointmentsPageModule} from '../pages/past-appointments/past-appointments.module';
import {ViewPrescriptionPageModule} from '../pages/view-prescription/view-prescription.module';
import { DatePipe } from '@angular/common'

// Test

import {TestPageModule} from '../pages/test/test.module';
import { TestProvider } from '../providers/test/test';
import { FcmProvider } from '../providers/fcm/fcm';

// My Modules


// For FCM

import { Firebase } from '@ionic-native/firebase';

import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';

const config = {
  // your firebase web config
  apiKey: "AIzaSyAaMmTjUzNLPR78IuitZ7iFYYTHzghEXgs",
  authDomain: "notifications-27153.firebaseapp.com",
  databaseURL: "https://notifications-27153.firebaseio.com",
  projectId: "notifications-27153",
  storageBucket: "notifications-27153.appspot.com",
  messagingSenderId: "247106393349"
}

@NgModule({
  declarations: [
    MyApp,
    HomePage,
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(config), 
    AngularFirestoreModule,
    HttpModule,
    HttpClientModule,
    LoginPageModule,
    SignupPageModule,
    AppointmentInfoModalPageModule,
    PrescriptionPageModule,
    FindDoctorPageModule,
    OrdersPageModule,
    EmergencyPageModule,
    SearchPageModule,
    DoctorMoreInfoPageModule,
    ConsultPageModule,
    PrescriptionCurrentPageModule,
    PrescriptionPastPageModule,
    OrdersOffersPageModule,
    OrdersPendingPageModule,
    OrdersPastPageModule,
    DoctorInfoPageModule,
    DoctorReviewPageModule,
    ConfirmDoctorPageModule,
    WorkplaceInfoPageModule,
    CurrentAppointmentsPageModule,
    PastAppointmentsPageModule, 
    ViewPrescriptionPageModule,
    AppointmentsPageModule,
    // AppointmentInfoModalPageModule,
    IonicModule.forRoot(MyApp, {
      // tabsHideOnSubPages:"true",
      scrollPadding: false,
      scrollAssist: true,
      autoFocusAssist: false
    }),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    // {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthProvider,
    PatientProvider,
    DatePipe,
    TestProvider,
    Firebase,
    FcmProvider,
  ]
})
export class AppModule {}

    
