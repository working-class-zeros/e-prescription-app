import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {DoctorReviewPage} from '../doctor-review/doctor-review';
import {DoctorInfoPage} from '../doctor-info/doctor-info';
// import { TestProvider } from '../../providers/test/test';
import { PatientProvider } from '../../providers/patient/patient';
import { HomePage } from '../home/home';
 /**
 * Generated class for the ConsultPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-consult',
  templateUrl: 'consult.html',
})
export class ConsultPage {
  infoPage = DoctorInfoPage;
  reviewPage = DoctorReviewPage;
  time:String;
  date:String = new Date().toLocaleString();
  doctor:any;
  workplaces=[];
  selectedIndex:number;
  relationship:any;
  reasonForVisit:String;
  constructor(public navCtrl: NavController, public navParams: NavParams, public patientServ: PatientProvider) {
    this.doctor = this.navParams.get('doctor');
    var addressTemp = this.doctor['addresses'];
    for(var i =0 ; i < addressTemp.length; i++){
      this.workplaces.push((addressTemp[i])['location']);
    }
    // console.log(this.doctor);
  }
  
  select(index){
    this.selectedIndex = index;
  }

  prepareDataToSend(){
    var JSONtoSend = {};
    JSONtoSend['address'] = (this.doctor['addresses'])[this.selectedIndex];
    JSONtoSend['_id'] = this.doctor['id'];
    JSONtoSend['date'] = this.date;
    JSONtoSend['time'] = this.time;
    JSONtoSend['reasonForVisit'] = this.reasonForVisit;
    console.log(JSONtoSend);
    this.patientServ.consult(JSONtoSend).then((res => {
      // console.log('appointment set');
      this.navCtrl.setRoot(HomePage);
    }), (err) => {
      console.log(err);
    });
  }

  test(){
    console.log("Date is")
    console.log(this.time)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConsultPage');
  }

}
