import { Component } from '@angular/core';
import { App, IonicPage, NavController, NavParams } from 'ionic-angular';
import { ConsultPage } from '../consult/consult';
import {WorkplaceInfoPage} from '../workplace-info/workplace-info';
/**
 * Generated class for the DoctorInfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-doctor-info',
  templateUrl: 'doctor-info.html',
})
export class DoctorInfoPage {

  doctor:any;
  workplaces = [];
  constructor(public navCtrl: NavController, public navParams: NavParams, public appCtrl: App) {
    this.doctor = this.navParams.data;
    // console.log(this.doctor);
    var addressTemp = this.doctor['addresses'];
    for(var i =0 ; i < addressTemp.length; i++){
      this.workplaces.push((addressTemp[i])['location']);
    }
  }

  goToConsult(){
    // this.navCtrl.pop();
    // Note:
    // We are doing this to access the parent's navctrl.
    // This page doctor-more-info has 2 tabs, info and review. info opens this page. So, this tab has a local navctrl.
    // We have to access the navctrl of doctor-more-info to pop and push consult page or you will have embedded pages
    this.appCtrl.getRootNav().pop();
    this.appCtrl.getRootNav().push(ConsultPage,{doctor:this.doctor});

    // this.navCtrl.push(ConsultPage,{doctor:this.doctor});
  }

  goToWorkplaceInfo(index){
    console.log(this.doctor);
    console.log(((this.doctor)['addresses'])[index]);
    this.navCtrl.push(WorkplaceInfoPage,{doctor:this.doctor, address: ((this.doctor)['addresses'])[index]});
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad DoctorInfoPage');
  }

}
