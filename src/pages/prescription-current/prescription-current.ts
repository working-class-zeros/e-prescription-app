import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {ViewPrescriptionPage} from '../view-prescription/view-prescription';

/**
 * Generated class for the PrescriptionCurrentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-prescription-current',
  templateUrl: 'prescription-current.html',
})
export class PrescriptionCurrentPage {

  prescriptions: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.prescriptions = (this.navParams.data);

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PrescriptionCurrentPage');
  }

  viewPrescription(prescription){
    this.navCtrl.push(ViewPrescriptionPage, {prescription:prescription});
  }
  test(){
    console.log(this.prescriptions);
  }

}
