import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PrescriptionCurrentPage } from './prescription-current';

@NgModule({
  declarations: [
    PrescriptionCurrentPage,
  ],
  imports: [
    IonicPageModule.forChild(PrescriptionCurrentPage),
  ],
})
export class PrescriptionCurrentPageModule {}
