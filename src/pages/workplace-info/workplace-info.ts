import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the WorkplaceInfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-workplace-info',
  templateUrl: 'workplace-info.html',
})
export class WorkplaceInfoPage {
  doctor:any;
  address:any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.doctor = this.navParams.get('doctor');
    this.address = this.navParams.get('address');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WorkplaceInfoPage');
  }

}
