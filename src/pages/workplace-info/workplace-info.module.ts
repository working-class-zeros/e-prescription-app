import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WorkplaceInfoPage } from './workplace-info';

@NgModule({
  declarations: [
    WorkplaceInfoPage,
  ],
  imports: [
    IonicPageModule.forChild(WorkplaceInfoPage),
  ],
})
export class WorkplaceInfoPageModule {}
