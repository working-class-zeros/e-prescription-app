import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the PastAppointmentsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-past-appointments',
  templateUrl: 'past-appointments.html',
})
export class PastAppointmentsPage {
  appointments: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.appointments = this.navParams.data;
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PastAppointmentsPage');
  }

}
