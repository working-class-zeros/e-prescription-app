import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OrdersPastPage } from './orders-past';

@NgModule({
  declarations: [
    OrdersPastPage,
  ],
  imports: [
    IonicPageModule.forChild(OrdersPastPage),
  ],
})
export class OrdersPastPageModule {}
