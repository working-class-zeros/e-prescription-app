import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PrescriptionPastPage } from './prescription-past';

@NgModule({
  declarations: [
    PrescriptionPastPage,
  ],
  imports: [
    IonicPageModule.forChild(PrescriptionPastPage),
  ],
})
export class PrescriptionPastPageModule {}
