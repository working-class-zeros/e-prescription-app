import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ViewPrescriptionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-view-prescription',
  templateUrl: 'view-prescription.html',
})
export class ViewPrescriptionPage {

  prescription : any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.prescription = this.navParams.get('prescription');
    console.log(this.prescription)
  }

  showMedicines(index){
    
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewPrescriptionPage');
  }

}
