import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppointmentInfoModalPage } from './appointment-info-modal';

@NgModule({
  declarations: [
    AppointmentInfoModalPage,
  ],
  imports: [
    IonicPageModule.forChild(AppointmentInfoModalPage),
  ],
})
export class AppointmentInfoModalPageModule {}
