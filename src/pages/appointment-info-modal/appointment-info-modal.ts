import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { PatientProvider } from '../../providers/patient/patient';

/**
 * Generated class for the AppointmentInfoModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-appointment-info-modal',
  templateUrl: 'appointment-info-modal.html',
})
export class AppointmentInfoModalPage {

  details:any;
  cancelFlag:any;
  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              public viewCtrl: ViewController,
              public patientService: PatientProvider) {
    this.details = this.navParams.get('details');
    this.cancelFlag = this.navParams.get('canceFlag');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AppointmentInfoModalPage');
  }

  cancelAppointment(){
    this.cancelFlag = true;
    this.patientService.cancelAppointment(this.details).then((res) => {
    }, (err) => {
      console.log(err);
    })
    // console.log("Cancel called")
    this.dismiss();
  }
  dismiss() {
    this.viewCtrl.dismiss({"cancelFlag":this.cancelFlag});
  }

}
