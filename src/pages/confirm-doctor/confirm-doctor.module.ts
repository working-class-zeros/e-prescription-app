import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConfirmDoctorPage } from './confirm-doctor';

@NgModule({
  declarations: [
    ConfirmDoctorPage,
  ],
  imports: [
    IonicPageModule.forChild(ConfirmDoctorPage),
  ],
})
export class ConfirmDoctorPageModule {}
