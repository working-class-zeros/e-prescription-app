import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DoctorMoreInfoPage } from './doctor-more-info';

@NgModule({
  declarations: [
    DoctorMoreInfoPage,
  ],
  imports: [
    IonicPageModule.forChild(DoctorMoreInfoPage),
  ],
})
export class DoctorMoreInfoPageModule {}
