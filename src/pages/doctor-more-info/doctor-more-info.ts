import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {DoctorReviewPage} from '../doctor-review/doctor-review';
import {DoctorInfoPage} from '../doctor-info/doctor-info';
/**
 * Generated class for the DoctorMoreInfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-doctor-more-info',
  templateUrl: 'doctor-more-info.html',
})
export class DoctorMoreInfoPage {
  infoPage = DoctorInfoPage;
  reviewPage = DoctorReviewPage;
  doctor:any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.doctor = this.navParams.get('doctor');
    // console.log('DoctorMoreInfo');
    // console.log(this.doctor);
    // console.log(this.doctor);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DoctorMoreInfoPage');
  }

}
