import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , LoadingController} from 'ionic-angular';
import { CurrentAppointmentsPage } from '../current-appointments/current-appointments';
import { PastAppointmentsPage} from '../past-appointments/past-appointments';
import moment from 'moment';
import { DatePipe } from '@angular/common'

import { PatientProvider } from '../../providers/patient/patient';
/**
 * Generated class for the AppointmentsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//   https://forum.ionicframework.com/t/date-object-from-string-in-dd-mm-yyyy-format/127326/2
// https://forum.ionicframework.com/t/how-to-compare-two-date/59145

@IonicPage()
@Component({
  selector: 'page-appointments',
  templateUrl: 'appointments.html',
})
export class AppointmentsPage {
  loading:any;
  currentAppointmentsPage = CurrentAppointmentsPage;
  pastAppointmentsPage = PastAppointmentsPage;
  future = [];
  past = [];
  // myDate: String = new Date("2018-08-04").toISOString().substring(0,10);
  appointments: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, 
    public patientService: PatientProvider, public datepipe: DatePipe,  public loadingCtrl: LoadingController) {
    // this.appointments = this.navParams.get('appointments');
    // this.segragate();
    // console.log(this.appointments);
    // console.log(this.myDate);
    // var someDate = this.appointments[0]['date'];
    // var dateObject = moment(someDate, "YYYY-MM-DD").toDate();
    // var dateObj2 = moment(this.myDate, "YYYY-MM-DD").toDate();
    // console.log(dateObject>= dateObj2)
    // console.log(dateObject);
    // console.log(dateObj2);
  }

  ionViewWillEnter(){
    // console.log("Will Enter of Appointments")
    this.getAppointments();

  }
 
  showLoader(){
 
    this.loading = this.loadingCtrl.create({
      content: 'Getting Appointments'
    });
 
    this.loading.present();
 
  }
  getAppointments() {
    this.showLoader()
    this.patientService.getAppointments().then((res) => {
      this.appointments = res;
      this.segragate();
      this.loading.dismiss();
      // console.log(res);
    }, (err) => {
      console.log(err);
    })
  }
  segragate(){

    // console.log("Segregating Appointments")
    if(this.appointments == undefined){
      return -1;
    }
    // var dateFull = new Date().toLocaleString();
    // console.log(dateFull + " is dateFull")
    var date = new Date();
    var today =this.datepipe.transform(date, 'dd/MM/yyyy');
    
    // today = dateFull.substring(0,10);
    console.log(today)
    var time = this.datepipe.transform(date, 'HH:mm');
    // var time = dateFull.substring(12,17);
    console.log("Time is "+time)
    var timeNow = moment(time,"HH:mm");
    console.log(timeNow);
    var todaysDate = moment(today,"DD-MM-YYYY")
    for(var i = 0; i < this.appointments.length; i++){
      var appDate = (this.appointments[i])['date'];
      var appTime = (this.appointments[i])['time'];
      console.log(appTime);
      appDate = moment(appDate,"YYYY-MM-DD")
      var diff = appDate.diff(todaysDate,'seconds');
      if(diff == 0){
        // Same day
        appTime = moment(appTime,'HH:mm');
        var timeDiff = appTime.diff(timeNow,'seconds');

        if(timeDiff >= 0){
          this.future.push(this.appointments[i]);
        }else{
          this.past.push(this.appointments[i]);
        }
      }else if(diff > 0){
        // Future
        this.future.push(this.appointments[i]);
      }else{
        this.past.push(this.appointments[i]);
      }

    }

    // console.log(this.past);
    // console.log(this.future);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AppointmentsPage');
  }

}
