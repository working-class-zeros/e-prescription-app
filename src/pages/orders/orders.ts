import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {OrdersOffersPage} from '../orders-offers/orders-offers';
import {OrdersPendingPage} from '../orders-pending/orders-pending';
import {OrdersPastPage} from '../orders-past/orders-past';
/**
 * Generated class for the OrdersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-orders',
  templateUrl: 'orders.html',
})
export class OrdersPage {

  offersPage = OrdersOffersPage;
  pendingPage = OrdersPendingPage;
  pastPage = OrdersPastPage;
  constructor(public navCtrl: NavController, public navParams: NavParams) {

  }



}
