import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {ConsultPage} from '../consult/consult';
import {DoctorMoreInfoPage} from '../doctor-more-info/doctor-more-info'
/**
 * Generated class for the SearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage {
  doctors: any;

  found:Boolean;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.doctors = this.navParams.get('doctors');
    // if((this.details).length )

  }

  
  goToConsultPage(docIndex){
    // console.log(this.details[docIndex]);

    this.navCtrl.push(ConsultPage,{doctor:this.doctors[docIndex]});
  }

  goToMoreInfoPage(docIndex){
    // console.log(this.details[docIndex]);

    this.navCtrl.push(DoctorMoreInfoPage,{doctor:this.doctors[docIndex]});
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchPage');
  }

}
