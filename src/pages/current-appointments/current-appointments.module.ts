import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CurrentAppointmentsPage } from './current-appointments';

@NgModule({
  declarations: [
    CurrentAppointmentsPage,
  ],
  imports: [
    IonicPageModule.forChild(CurrentAppointmentsPage),
  ],
})
export class CurrentAppointmentsPageModule {}
