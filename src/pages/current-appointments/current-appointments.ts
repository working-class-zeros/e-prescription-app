import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the CurrentAppointmentsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-current-appointments',
  templateUrl: 'current-appointments.html',
})
export class CurrentAppointmentsPage {
  appointments : any
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.appointments = this.navParams.data; // Data send using [rootParams] property of tab. So no "get()" function
    console.log(this.appointments);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CurrentAppointmentsPage');
  }

}
