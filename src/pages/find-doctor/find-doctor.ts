import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { PatientProvider } from '../../providers/patient/patient';
import { SearchPage } from '../search/search';
/**
 * Generated class for the FindDoctorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-find-doctor',
  templateUrl: 'find-doctor.html',
})
export class FindDoctorPage {
  doctors: any;
  searchTerm: String;
  loading: any;
  history: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public authService: AuthProvider,
    public patientServ: PatientProvider, public alertCtrl: AlertController, public loadingCtrl: LoadingController) {
      this.getHistory();

  }

  doAlert() {
    let alert = this.alertCtrl.create({
      title: "No Results Found :(",
      subTitle: "Try again!",
      buttons: ["OK"]
    });
    alert.present();
  }

  showLoader() {

    this.loading = this.loadingCtrl.create({
      content: 'Searching Doctor'
    });

    this.loading.present();

  }

  searchDoctor() {
    this.showLoader();
    this.patientServ.search(this.searchTerm).then((result) => {
      this.doctors = result;
      this.loading.dismiss();
      if ((this.doctors)['status'] == 404) {
        this.doAlert();
        return;
      }
      this.navCtrl.push(SearchPage, { doctors: this.doctors });

    })
  }

  getHistory() {
    this.patientServ.getHistory().then((res) => {
      this.history = res;
      console.log(this.history);
    }, (err) => {
      console.log(err);
    })
  }
  ionViewDidLoad() {

  }



}
