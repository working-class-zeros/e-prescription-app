import { Component } from '@angular/core';
import { NavController, ModalController } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { AuthProvider } from '../../providers/auth/auth';
import { PrescriptionPage } from '../prescription/prescription';
import { FindDoctorPage } from '../find-doctor/find-doctor';
import { OrdersPage } from '../orders/orders';
import { EmergencyPage } from '../emergency/emergency';
import { AppointmentInfoModalPage } from '../appointment-info-modal/appointment-info-modal'
import { TestPage } from '../test/test';
import { PatientProvider } from '../../providers/patient/patient';
import { AppointmentsPage } from '../appointments/appointments';
import { DatePipe } from '@angular/common'

import moment from 'moment';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  appointments = [];
  tempAppointments: any;  
  cancelFlag = false;
  loading: any;
  constructor(public navCtrl: NavController, public authService: AuthProvider, public patientService: PatientProvider,
    public modalCtrl: ModalController, public datepipe: DatePipe) {
    // console.log(this.appointments);
  }

  ionViewWillEnter() {
    this.getAppointments();
  }
  openAppointmentModel(index) {
    let myModal = this.modalCtrl.create(AppointmentInfoModalPage, { 'details': this.appointments[index], cancelFlag: this.cancelFlag });
    myModal.present();
    myModal.onDidDismiss(cancelFlag => {
      // Do things with data coming from modal, for instance :
      if (cancelFlag)
        this.getAppointments();
    });
  }

  getAppointments() {
    this.patientService.getAppointments().then((res) => {
      this.tempAppointments = res;
      this.segragate();

      // console.log(res);
    }, (err) => {
      console.log(err);
      this.loading.dismiss();

    })
  }

  segragate() {
    this.appointments = [];
    // var dateFull = new Date().toLocaleString();
    // var today = dateFull.substring(0, 10);
    var date = new Date();
    var today = this.datepipe.transform(date, 'dd/MM/yyyy');
    // var time = dateFull.substring(12, 17);
    // console.log("TOday Is" + today)
    var time = this.datepipe.transform(date, 'HH:mm');
    var timeNow = moment(time, "HH:mm")
    var todaysDate = moment(today, "DD-MM-YYYY")
    for (var i = 0; i < this.tempAppointments.length; i++) {
      if ((this.tempAppointments[i])['status'] != "Done" && (this.tempAppointments[i])['status'] != "Cancelled") { //ENUM-IFY
        // console.log((this.tempAppointments[i])['status'])
        var appDate = (this.tempAppointments[i])['date'];
        var appTime = (this.tempAppointments[i])['time'];
        appDate = moment(appDate, "YYYY-MM-DD")
        // console.log(todaysDate + "Is Today's date")
        var diff = appDate.diff(todaysDate, 'seconds');
        if (diff == 0) {
          // Same day
          appTime = moment(appTime, 'HH:mm');
          var timeDiff = appTime.diff(timeNow, 'seconds');
          if (timeDiff >= 0) {
            this.appointments.push(this.tempAppointments[i]);
          } else {
            // Add code for "DONE" here
          }
        } else if (diff > 0) {
          // Future
          this.appointments.push(this.tempAppointments[i]);
        }
      }
    }

  }
  goToPrescriptions() {
    this.navCtrl.push(PrescriptionPage);
  }

  goToFindDoctor() {
    this.navCtrl.push(FindDoctorPage);
  }

  goToOrders() {
    this.navCtrl.push(OrdersPage);
  }

  goToEmergency() {
    this.navCtrl.push(EmergencyPage);

  }

  goToTestPage() {
    this.navCtrl.push(TestPage);
  }

  goToAppointments() {
    this.navCtrl.push(AppointmentsPage)
  }
  logout() {
    this.authService.logout();
    this.navCtrl.setRoot(LoginPage);
  }

  

}
