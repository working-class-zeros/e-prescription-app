import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DoctorReviewPage } from './doctor-review';

@NgModule({
  declarations: [
    DoctorReviewPage,
  ],
  imports: [
    IonicPageModule.forChild(DoctorReviewPage),
  ],
})
export class DoctorReviewPageModule {}
