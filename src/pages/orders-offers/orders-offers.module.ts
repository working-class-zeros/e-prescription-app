import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OrdersOffersPage } from './orders-offers';

@NgModule({
  declarations: [
    OrdersOffersPage,
  ],
  imports: [
    IonicPageModule.forChild(OrdersOffersPage),
  ],
})
export class OrdersOffersPageModule {}
