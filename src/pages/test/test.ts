import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import {TestProvider} from '../../providers/test/test';
/**
 * Generated class for the TestPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-test',
  templateUrl: 'test.html',
})
export class TestPage {

  
  // details:any;
  loading:any;
  email:string;
  password:string;
  name: string;
  dob:string;
  rating:Number;
  addresses=[];
  qualifications=[];
  speciality:String;
  bio:String;
  constructor(public navCtrl: NavController, public navParams: NavParams, public testServ : TestProvider, public loadingCtrl : LoadingController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TestPage');
  }


  registerDoctor(){
    this.showLoader();
    var add1 = {
      name: "Clinic Hash 1",
      address: "Roop Mahal, Prem Gali, Kholi Number 420",
      timing: ["0900-0100"],
      location: "Magadi Rd"
    }
    var add2 = {
      name: "Clinic Hash 2",
      address: "Kholi Number 420",
      timing: ["0900-1100"],
      location: "Jayanagar"
    }
    this.addresses.push(add1);
    this.addresses.push(add2);
    console.log("DONE");
    this.qualifications.push('M.B.B.S');
    this.qualifications.push('MD in ENT');
    let details = {
      email: this.email,
      password : this.password,
      name: this.name,
      dob: this.dob,
      rating: this.rating,
      addresses : this.addresses,
      qualifications:this.qualifications,
      speciality:this.speciality,
      bio:this.bio
    }
    this.testServ.createDoctorAccount(details).then((result) => {
      this.loading.dismiss();
      console.log(result);
    }, (err) => {
        this.loading.dismiss();
        console.log(err);
    });
 
  }

  showLoader(){
 
    this.loading = this.loadingCtrl.create({
      content: 'Authenticating...'
    });
 
    this.loading.present();
 
  }

}
