import { Component, ViewEncapsulation } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import {PrescriptionCurrentPage} from '../prescription-current/prescription-current';
import { PrescriptionPastPage } from '../prescription-past/prescription-past';
import { PatientProvider } from '../../providers/patient/patient';
/**
 * Generated class for the PrescriptionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-prescription',
  templateUrl: 'prescription.html',
  encapsulation: ViewEncapsulation.None
})
export class PrescriptionPage {

  currentPage = PrescriptionCurrentPage;
  pastPage = PrescriptionPastPage;
  prescriptions = [];
  res:any;
  loading:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public patientService : PatientProvider,
    public loadingCtrl: LoadingController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PrescriptionPage');
    // this.getPrescriptions();
  }

  ionViewWillEnter(){
    this.getPrescriptions();
  }

  showLoader() {
    console.log("SHOW LOADER")
    this.loading = this.loadingCtrl.create({
      content: 'Loading Prescriptions'
    });

    this.loading.present();

  }

  getPrescriptions(){
    this.showLoader();
    this.patientService.getPrescriptions().then((res) => {
      this.res = res;
      for(var x = 0; x < this.res.length; x++){
        this.prescriptions.push(res[x]);
      }
      // console.log(this.prescriptions);
      this.loading.dismiss()
    }, (err) => {
      console.log(err)
    })
  }
}
