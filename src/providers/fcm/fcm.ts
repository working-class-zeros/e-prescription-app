
import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';

import { Firebase } from '@ionic-native/firebase';
import { Platform } from 'ionic-angular';

@Injectable()
export class FcmProvider {

  constructor(private platform: Platform, 
              public firebaseNative:Firebase,
              public http: Http) {
    console.log('Hello FcmProvider Provider');
  }

  token: any;
  // Get permission from the user
  async getToken() {

    if (this.platform.is('android')) {
      this.token = await this.firebaseNative.getToken();
      console.log("The Token is "+ this.token);
    }

    if (this.platform.is('ios')) {
      this.token = await this.firebaseNative.getToken();
      await this.firebaseNative.grantPermission();
    }

    if (this.platform.is('core')){
      return " "
    }
    
  }

  // Listen to incoming FCM messages
  listenToNotifications() {
    return this.firebaseNative.onNotificationOpen()
  }
}