// import { HttpClient } from '@angular/common/http';
// import { Injectable } from '@angular/core';


// @Injectable()
// export class AuthProvider {

//   constructor(public http: HttpClient) {
//     console.log('Hello AuthProvider Provider');
//   }

// }

/* Our Code */

import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';
import {CONFIG} from '../../../config/config';
import {FcmProvider} from '../fcm/fcm';

import { Firebase } from '@ionic-native/firebase';
import { Platform } from 'ionic-angular';


@Injectable()
export class AuthProvider {
  public notifToken: any;
  public token: any;
  public API_URL = CONFIG['API_URL'];
  constructor(public http: Http, public storage: Storage, public FcmService : FcmProvider,
    private platform: Platform, 
    public firebaseNative:Firebase,) {
    // console.log(CONFIG['API_URL'])
  }
  async getToken(details) {

    if (this.platform.is('android')) {
      this.notifToken = await this.firebaseNative.getToken();
      console.log("The Token is "+ this.notifToken);
    }

    if (this.platform.is('ios')) {
      this.notifToken = await this.firebaseNative.getToken();
      await this.firebaseNative.grantPermission();
    }
    details['notifToken'] = this.notifToken;
    this.createAccount(details)
  }
    

  
  checkAuthentication(){
 
    return new Promise((resolve, reject) => {
 
        //Load token if exists
        this.storage.get('token').then((value) => {
 
            this.token = value;
 
            let headers = new Headers();
            headers.append('Authorization', this.token);
 
            this.http.get(this.API_URL+'/protec', {headers: headers})
                .subscribe(res => {
                    resolve(res);
                }, (err) => {
                    reject(err);
                });
 
        });        
 
    });
 
  }
 
  createAccount(details){
 
    return new Promise((resolve, reject) => {
 
        
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        // console.log("Details are ");
        // console.log(details);
        this.http.post(this.API_URL+'/patient/register', JSON.stringify(details), {headers: headers})
          .subscribe(res => {
 
            let data = res.json();
            this.token = data.token;
            this.storage.set('token', data.token);
            resolve(data);
 
          }, (err) => {
            reject(err);
          });
 
    });
 
  }
 
  login(credentials){
 
    return new Promise((resolve, reject) => {
 
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
 
        this.http.post(this.API_URL+'/patient/login', JSON.stringify(credentials), {headers: headers})
          .subscribe(res => {
 
            let data = res.json();
            this.token = data.token;
            this.storage.set('token', data.token);
            console.log(this.token);
            resolve(data);
 
            resolve(res.json());
          }, (err) => {
            reject(err);
          });
 
    });
 
  }
 
  logout(){
    console.log("Logging Out")
    this.storage.set('token', '');
  }
 
}
