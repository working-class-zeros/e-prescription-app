import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';
import { CONFIG } from '../../../config/config';
import { AuthProvider } from '../auth/auth';

/*
  Generated class for the TestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class TestProvider {

  public token: any;
  public API_URL = CONFIG['API_URL'];
  constructor(public http: Http, public storage: Storage, public authService: AuthProvider) {
    // console.log(CONFIG['API_URL'])
  }


  checkAuthentication() {

    return new Promise((resolve, reject) => {

      //Load token if exists
      this.storage.get('token').then((value) => {

        this.token = value;

        let headers = new Headers();
        headers.append('Authorization', this.token);

        this.http.get(this.API_URL + '/protec', { headers: headers })
          .subscribe(res => {
            resolve(res);
          }, (err) => {
            reject(err);
          });

      });

    });

  }

  createDoctorAccount(details) {

    return new Promise((resolve, reject) => {

      let headers = new Headers();
      headers.append('Content-Type', 'application/json');

      this.http.post(this.API_URL + '/doctor/register', JSON.stringify(details), { headers: headers })
        .subscribe(res => {

          let data = res.json();
          this.token = data.token;
          this.storage.set('token', data.token);
          resolve(data);

        }, (err) => {
          reject(err);
        });

    });

  }

  login(credentials) {

    return new Promise((resolve, reject) => {

      let headers = new Headers();
      headers.append('Content-Type', 'application/json');

      this.http.post(this.API_URL + '/doctor/login', JSON.stringify(credentials), { headers: headers })
        .subscribe(res => {

          let data = res.json();
          this.token = data.token;
          this.storage.set('token', data.token);
          resolve(data);

          resolve(res.json());
        }, (err) => {
          reject(err);
        });
      // console.log()

    });

  }

  logout() {
    this.storage.set('token', '');
  }

  // consult(details) {

  //   return new Promise((resolve, reject) => {

  //     let headers = new Headers();
  //     headers.append('Content-Type', 'application/json');
  //     // console.log(details);
  //     headers.append('Authorization', this.authService.token);

  //     this.http.post(this.API_URL + '/patient/consult/' + details['_id'], JSON.stringify(details), { headers: headers })
  //       .subscribe(res => {

  //         let data = res.json();
  //         // this.token = data.token;
  //         // this.storage.set('token', data.token);
  //         resolve(data);

  //         resolve(res.json());
  //       }, (err) => {
  //         reject(err);
  //       });

  //   });

  // }

}
