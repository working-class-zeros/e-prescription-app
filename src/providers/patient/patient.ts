import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { AuthProvider } from '../auth/auth';
import { CONFIG } from '../../../config/config';
/*
  Generated class for the PatientProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PatientProvider {
  API_URL = CONFIG['API_URL'];
  constructor(public http: Http, public authService: AuthProvider) {
  }

  search(name) {
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append('Authorization', this.authService.token);
      name = name.replace(" ", "+");
      this.http.get(this.API_URL + '/searchdoctor/' + name, { headers: headers })
        .map(res => res.json())
        .subscribe(data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }

  consult(details) {

    return new Promise((resolve, reject) => {

      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      // console.log(details);
      headers.append('Authorization', this.authService.token);

      this.http.post(this.API_URL + '/patient/consult/' + details['_id'], JSON.stringify(details), { headers: headers })
        .subscribe(res => {

          let data = res.json();
          // this.token = data.token;
          // this.storage.set('token', data.token);
          resolve(data);

          resolve(res.json());
        }, (err) => {
          reject(err);
        });

    });

  }

  getAppointments() {
    return new Promise((resolve, reject) => {

      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      // console.log(details);
      headers.append('Authorization', this.authService.token);

      this.http.get(this.API_URL + '/patient/appointments', { headers: headers })
        .subscribe(res => {
          let data = res.json();
          resolve(data);
        }, (err) => {
          reject(err);
        });

    });
  }

  cancelAppointment(details){
    return new Promise((resolve,reject) => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('Authorization', this.authService.token);
      // console.log(this.authService.token);
      this.http.post(this.API_URL+"/patient/appointment/"+details['appointmentId']+"/cancel", JSON.stringify(details),{headers:headers})
        .subscribe(res => {
          let data = res.json();
          resolve(data);
        }, (err) => {
          reject(err);
        })
    })
  }

  getPrescriptions(){
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      // console.log(details);
      headers.append('Authorization', this.authService.token);

      this.http.get(this.API_URL + '/patient/prescriptions', { headers: headers })
        .subscribe(res => {

          let data = res.json();
          resolve(data);
        }, (err) => {
          reject(err);
        });

    });
  }

  getHistory(){
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      // console.log(details);
      headers.append('Authorization', this.authService.token);

      this.http.get(this.API_URL + '/patient/history', { headers: headers })
        .subscribe(res => {
          let data = res.json();
          resolve(data);
        }, (err) => {
          reject(err);
        });

    });
  }
}
