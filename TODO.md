# Informal List of things we comtemplate to do


## Remaining

* Create dummy cards in prescription (change once doctor-app is able to add prescription)
* Check login.html button styling. Inline styling works, but classes doesn't. :/
* Go through how to get Rating module and Date-time picker module integrated
* Hardcoded the address field and qualification field in add-doctor; change
* Form validation in Consult page.
* Figure out a method to push notifications to selective users. Case in point : When patient wants to consult one particular doctor
* Fix Consulting from History (New Page?)
* Automatically mark appointments as cancelled/past when the date passes.
* Fix history in API to get the latest 5 instead of first 5 visits

## Done
* Modify sign-up page to take more params not only email and password - DONE
* Dummy UI in Orders and Emergency =>  Basic UI done.
* Finish designing Consult Doctor More Info Page page. More data needed, depends on doctor-view. Dummy Data added and page designed => Update #2 : Finished with non-dummy data except address. Need to implement addresses in doctor signup app.

* In Prescription view, tab doesnt work. WHY? (Try having a button that redirects to PrescriptionCurrentPage) => SOLN : Check Prescriptions Page. (While referencing the two pages that tab must be open used ":" instead of "=")
* Design Consult Page
* In doctor model, add timing to every address and not as a common field => Fix : Schema changed
* Checking for past and present appointments have been done only using date. Do using time also
* Change date formats to DDMMYYYY from YYYYMMDD
* In home, don't show old pending appointments. 
* Test and host on Heroku (v0.0.7) 
* Figure out how to clean and display only active appointments or only past appointments or only pending appointments in patient side




